provider "aws" {
  alias      = "gov-west"
  region     = var.region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

module "unicorn_sql" {
  source = "./modules/leidos/sql"
  vpc_id = "vpc-083cce3f73cd9de84"
  aws_subnet_id = var.AWS_SUBNET_ID
  aws_subnet_side = var.AWS_SUBNET_AB
  aws_tenant = var.TENANT
}

