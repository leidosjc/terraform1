variable "AWS_ACCESS_KEY" {
    default = ""
}

variable "AWS_SECRET_KEY" {
    default = ""
}

variable "AWS_SUBNET_ID" {
    default = ""
}

variable "AWS_SUBNET_AB" {
    default = "A"
}

variable "TENANT" {
    default = "Foxtrot"
}

variable "region" {
  type     = string
  default = "us-gov-west-1"
}

