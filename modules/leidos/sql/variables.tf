variable "aws_subnet_id" {
  description = "AWS Subnet ID. Example ('subnet-000000000000000') "
  type        = string
}

variable "aws_tenant" {
  description = "HAART Name For Subnet. Example (alpha|bravo|...) "
  type        = string    
}

variable "aws_subnet_side" {
  description = "A or B"
  default     = "A"
  type        = string    
}

variable "region" {
  type = string
  default = "us-gov-west-1"
}

variable "vpc_id" {
  description = "VPC ID of where the application is to be installed"
  default = "vpc-083cce3f73cd9de84"
}
