
output "unicorn_sql_ip" {
  value       = aws_instance.unicorn_sql.private_ip
  description = "The IP Address of the UNICORN SQL Server"
}
