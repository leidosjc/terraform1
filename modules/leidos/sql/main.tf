resource "aws_security_group" "sql-nodes" {
  name        = "unicorn-sql-sg"
  description = "Allow public subnet to access app"
  vpc_id     = var.vpc_id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/8"]
        description = "ssh from a single ip address"
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/8"]
        description = "open ports from everywhere"
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/8"]
        description = "open ports from everywhere"
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        description = "open ports to anywhere"
    }
  tags = {
    Name = "UNICORN-SQL-SG"
  }
}

resource "aws_instance" "unicorn_sql" {
  ami                    = "ami-0b29ebc56e541b732"
  instance_type          = "t2.micro"
  subnet_id              = var.aws_subnet_id
  security_groups        = [aws_security_group.sql-nodes.id]

  tags = {
    Name    = "${var.aws_tenant} - Private${var.aws_subnet_side} - unicorn sql 2012"
    OEM     = "Leidos"
    Tenant  = var.aws_tenant
    Subnet  = "Private${var.aws_subnet_side}"
    Version = "2012"
  }
}